package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/joho/godotenv"
)

var wg sync.WaitGroup

func main() {
	godotenv.Load()
	csvFilename := flag.String("csv", "country.csv", "file containing list of countries for covid data")
	flag.Parse()

	file, err := os.Open(*csvFilename)
	if err != nil {
		exit(fmt.Sprintf("failed to open the csv file %s\n", *csvFilename))
	}
	//r := csv.NewReader(file)
	key := os.Getenv("KEY")
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var list []string

	for scanner.Scan() {
		list = append(list, strings.ToUpper(scanner.Text()))
	}
	for i, country := range list {
		fmt.Printf("%d. %s\n", i+1, country)
	}
	fmt.Println("Enter country name:")
	var val string
	fmt.Scanf("%s", &val)
	// val = "india"
	val = strings.ToUpper(val)
	countryFound := find(list, val)
	country := make(chan string)
	wg.Add(2)
	go func() {
		if countryFound {
			fmt.Printf("Country %s found. Data fetching please wait...\n", val)
			go API(val, key)
			<-country
		} else {
			fmt.Printf("Country %s not found. Please enter from list above", val)
		}
		wg.Done()
	}()
	wg.Wait()

}

func find(countries []string, val string) bool {
	for i := range countries {
		if countries[i] == val {
			return true
		}
	}
	return false
}

func API(val string, key string) {
	url := "https://covid-19-coronavirus-statistics2.p.rapidapi.com/countriesData"

	req, _ := http.NewRequest("GET", url, nil)

	// req.Header.Add("content-type", "application/json")
	q := req.URL.Query()
	q.Add("name", val)
	req.URL.RawQuery = q.Encode()
	req.Header.Add("x-rapidapi-host", "covid-19-coronavirus-statistics2.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", key)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	var responseObject Response
	err = json.Unmarshal(body, &responseObject)
	if err != nil {
		fmt.Println(err)
	}
	// fmt.Printf("API Response as struct %+v\n", responseObject)
	for _, v := range responseObject.Result {
		CsvCountry := strings.ToUpper(v.Country)
		if CsvCountry == val {
			data, err := json.Marshal(v)
			if err != nil {
				fmt.Println(err)
			}
			ioutil.WriteFile("covid.json", data, 0644)
		}
	}
	fmt.Println("File Writed")

}

type Response struct {
	Success bool         `json:"success"`
	Result  []ResultData `json:"result"`
}
type ResultData struct {
	Country        string `json:"country"`
	TotalCases     string `json:"totalCases"`
	NewCases       string `json:"newCases"`
	TotalDeaths    string `json:"totalDeaths"`
	NewDeaths      string `json:"newDeaths"`
	TotalRecovered string `json:"totalRecovered"`
	ActiveCases    string `json:"activeCases"`
}

func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)

}